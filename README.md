# Description

`pure` is a library that allow you to import in your bash scripts, all the functions from the [Pure Bash Bible](https://github.com/dylanaraps/pure-bash-bible).

**I did not write a single meaningful line of code, all the credit goes to the people that did write the Bible in the first place.**

I did some tests and importing the library just takes **0.002 seconds**, so having all the functions in one place won't make your scripts slower.

All the functions start with the prefix `pure.` in order to not interfer with your own functions.

For more information please go directly to the original [repository](https://github.com/dylanaraps/pure-bash-bible).

Last time updated: 12 July 2018

# How to install ? 

## Instructions

Write in your terminal:
```bash
git clone "https://gitlab.com/gawlk/pure.git"
cd 
bash install.sh
```

# How to use ?

## Add the library

Add the following line at the top of your bash scripts:
```bash
. pure
```

## List of available functions

Write in your terminal:
```bash
. pure
pure.get_functions pure
```
